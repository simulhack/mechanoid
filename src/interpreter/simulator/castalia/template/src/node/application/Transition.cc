@IncludeClassHeader

#include <utility>

#include "uppaal_globals.h"
@IncludeUppaalPacket

@ClassName::@ClassName(std::string name,
                       std::string sourceStateID,
                       std::string targetStateID,
                       VirtualApplication *application)
        : AbstractTransition(std::move(name), std::move(sourceStateID), std::move(targetStateID), application)
{
}

bool @ClassName::testGuard() const
{
    // The guard
    @Guard
}

bool @ClassName::doSynchronization() {
    // The synchronization
    @Synchronization
}

std::string @ClassName::doAssignments()
{
    // The assignments
    @Assignments

    return targetStateID_;
}
