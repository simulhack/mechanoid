@IncludeHeader

Define_Module(@DefineModule);

void @ClassName::startup()
{
    processingPeriodMillis = ((double)par("processingPeriodMillis") / 1000.0);
    setTimer(CHANGE_STATE, processingPeriodMillis * genk_dblrand(0));

    @InitializeUppalTemplate

    trace() << "Startup done";
}

void @ClassName::timerFiredCallback(int index)
{
    trace() << "Executing on tick timeout";
    switch (index)
    {
        case CHANGE_STATE:
        {
            setTimer(CHANGE_STATE, processingPeriodMillis);

            trace() << "Initial state " << uppaalTemplate.getCurrentStateID().c_str();
            uppaalTemplate.evolve();
            trace() << "Final state " << uppaalTemplate.getCurrentStateID().c_str();

        }
        break;
    }
}

void @ClassName::fromNetworkLayer(ApplicationPacket *applicationPacket, const char *source, double rssi, double lqi)
{
    trace() << "Packet received from network layer";

    UppaalPacket *uppaalPacket = check_and_cast<UppaalPacket*>(applicationPacket);
    rxBuffer.push_back(uppaalPacket->getUppaalData());
    trace() << "UppaalPacket received from node" << uppaalPacket->getUppaalData().senderNodeID << " stored in rxBuffer";

}

void @ClassName::finishSpecific()
{
}

void @ClassName::traceMessage(std::string message)
{
    trace() << message.c_str();
}

@Methods
